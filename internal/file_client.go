package internal

import (
	"context"

	"gitlab.com/ruijc/storage/core/file"
	"google.golang.org/grpc"
)

type FileClient interface {
	Initiate(ctx context.Context, in *file.InitiateReq, opts ...grpc.CallOption) (*file.InitiateRsp, error)

	Abort(ctx context.Context, in *file.AbortReq, opts ...grpc.CallOption) (*file.AbortRsp, error)

	Complete(ctx context.Context, in *file.CompleteReq, opts ...grpc.CallOption) (*file.CompleteRsp, error)
}
