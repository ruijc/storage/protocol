package internal

import (
	"context"

	"github.com/gabriel-vasile/mimetype"
	"github.com/goexl/exception"
	"github.com/goexl/gox/field"
	"github.com/goexl/gox/http"
	"github.com/goexl/hud"
	"gitlab.com/ruijc/storage/core/file"
	"gitlab.com/ruijc/storage/core/kernel"
)

type Lifecycle struct {
	context  context.Context
	client   FileClient
	filename string
	id       *uint64
}

func NewLifecycle(ctx context.Context, client FileClient, filename string, id *uint64) *Lifecycle {
	return &Lifecycle{
		context:  ctx,
		client:   client,
		filename: filename,
		id:       id,
	}
}

func (l *Lifecycle) Initiate(parts int, start int, mime *mimetype.MIME) (urls []*hud.Url, err error) {
	req := new(file.InitiateReq)
	req.Mime = mime.String()
	req.Name = l.filename
	req.Parts = int32(parts)
	req.Start = int32(start)
	if rsp, ie := l.client.Initiate(l.context, req); nil != ie {
		err = ie
	} else {
		*l.id = rsp.Id
		urls = l.convert(rsp.Urls)
	}

	return
}

func (l *Lifecycle) Abort() (err error) {
	req := new(file.AbortReq)
	req.Id = *l.id
	if rsp, ae := l.client.Abort(l.context, req); nil != ae {
		err = ae
	} else if 0 == rsp.Id {
		err = exception.New().Message("取消上传出错").Field(field.New("id", req.Id)).Build()
	}

	return
}

func (l *Lifecycle) Complete(parts []*hud.Part) (err error) {
	req := new(file.CompleteReq)
	req.Id = *l.id
	req.Parts = make([]*kernel.Part, 0, len(parts))
	for _, _part := range parts {
		part := new(kernel.Part)
		part.Number = _part.Number
		part.Etag = _part.Header.Get("ETag")
		part.Size = _part.Size.Byte()
		req.Parts = append(req.Parts, part)
	}
	if rsp, ce := l.client.Complete(l.context, req); nil != ce {
		err = ce
	} else if nil == rsp.File {
		err = exception.New().Message("完成上传出错").Field(field.New("parts", parts)).Build()
	}

	return
}

func (l *Lifecycle) convert(urls []*kernel.Url) (to []*hud.Url) {
	to = make([]*hud.Url, 0, len(urls))
	for _, from := range urls {
		url := new(hud.Url)
		url.Method = http.ParseMethod(from.Method)
		url.Target = from.Target
		to = append(to, url)
	}

	return
}
