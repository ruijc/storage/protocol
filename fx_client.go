package protocol

import (
	"context"
	"path/filepath"

	"github.com/goexl/gox"
	"github.com/goexl/http"
	"github.com/goexl/hud"
	"github.com/goexl/log"
	"gitlab.com/ruijc/storage/protocol/internal"
	"google.golang.org/grpc"
)

// FxClient 增强版客户端
type FxClient struct {
	FileClient

	transfer *hud.Transfer
	_        gox.Pointerized
}

// NewFxClient 创建增强版客户端
func NewFxClient(connection grpc.ClientConnInterface, http *http.Client, logger log.Logger) *FxClient {
	return &FxClient{
		FileClient: NewFileClient(connection),
		transfer:   hud.New().Http(http).Logger(logger).Build(),
	}
}

func (c *FxClient) Put(ctx context.Context, filename string, bytes []byte) (id uint64, err error) {
	life := internal.NewLifecycle(ctx, c.FileClient, filename, &id)
	uploader := c.transfer.Upload().Bytes(bytes).Multipart().Lifecycle(life).Build()
	err = uploader.Build().Do()

	return
}

func (c *FxClient) Upload(ctx context.Context, path string) (id uint64, err error) {
	_, filename := filepath.Split(path)
	life := internal.NewLifecycle(ctx, c.FileClient, filename, &id)
	uploader := c.transfer.Upload().Filepath(path).Multipart().Lifecycle(life).Build()
	err = uploader.Build().Do()

	return
}
